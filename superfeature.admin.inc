<?php

/**
 * @return array
 */
function superfeature_form() {
  $form = array(
    '#tree' => TRUE,
    'features' => array(),
  );

  foreach (superfeature_get_features() as $feature) {
    $description = array();

    if ($feature->superfeature['enabled']) {
      $description[] = 'Feature is Enabled. Disable to include to Superfeature.';
    }
    if ($feature->superfeature['unsupported components']) {
      $description[] = 'Feature has unsupported components: ' . implode(', ', $feature->superfeature['unsupported components']) . '.';
    }

    $form['features'][$feature->name] = array(
      '#type' => 'checkbox',
      '#title' => $feature->info['name'] . ' (' . $feature->name . ')',
      '#disabled' => !empty($description),
      '#description' => $description ? theme('item_list', array('items' => $description)) : '',
      '#default_value' => superfeature_feature_is_enabled($feature),
    );
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Save',
  );

  uasort($form['features'], function ($a, $b) {
    return strcmp($b['#disabled'] ? '0' : '1', $a['#disabled'] ? '0' : '1');
  });

  return $form;
}

/**
 * @param $form
 * @param $form_state
 */
function superfeature_form_submit($form, $form_state) {
  variable_set('superfeature', $form_state['values']['features']);
}
